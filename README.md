# epi-puma_censo_inegi_2020_node

## Descripcion

Este repositorio contiene el microservicio del censo de INEGI 2020 para la plataforma de epi-puma.


## Requerimientos

* NodeJS >= 12.0
* npm >= 6.14

### DB

* PostgreSQL >= 12.3

Para desplegar este microservicio se necesita tener el respaldo de la base de datos correspondiente, construida apartir de los datos de INEGI 2020.

## Como desplegar

Para instalar las dependencias

```
	$ npm install
```

Para echarlo a andar

```
	$ npm start
```

## Admin

* zs10011598@gmail.com