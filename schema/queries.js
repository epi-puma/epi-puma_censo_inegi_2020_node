const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo_censo_inegi_2020(message: String): String,
    all_censo_inegi_2020_covariables(limit: Int!, filter: String!): [CovariableCensoINEGI2020!]!
    ocurrence_censo_inegi_2020_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrenceCensoINEGI2020!]!
  
  }
`;

module.exports = schema;