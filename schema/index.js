const { mergeTypeDefs } = require('@graphql-tools/merge');
const queries = require('./queries.js');
const types = require('./types');
const interfaces = require('./interfaces');

let schema = interfaces;
schema = schema.concat(types);
schema.push(queries);
const merged_types = mergeTypeDefs(schema);
module.exports = merged_types;
