const { gql } = require('apollo-server');

module.exports = gql`
  type CovariableCensoINEGI2020 implements Covariable @key(fields: "id") {
	id: ID!,
	name: String!,
	interval: String,
	bin: Int!,
	code: String!,
	lim_inf: Int!,
	lim_sup: Int!,
  mesh: String,
	cells_state: [String!],
	cells_mun: [String!],
	cells_ageb: [String!]
  }
`;
