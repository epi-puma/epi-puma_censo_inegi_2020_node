const { gql } = require('apollo-server');

module.exports = gql`
  interface Covariable {
    id: ID
    cells_state: [String!]
    cells_mun: [String!]
    cells_ageb: [String!]
  }
`;