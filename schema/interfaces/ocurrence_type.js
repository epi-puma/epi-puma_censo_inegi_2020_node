const { gql } = require('apollo-server');

module.exports = gql`
  interface Ocurrence {
    id: ID!
    gridid_state: String
    gridid_mun: String
    gridid_ageb: String
  }
`;
